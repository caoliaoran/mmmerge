The Community branch of the MMMerge project - Might and Magic Merge, also known as the World of Enroth.
Check the thread here: https://www.celestialheavens.com/forum/10/16657

# WILL ONLY WORK WITH MERGE VERSION 08.10.2019 !

### Installation instructions -

0. Install the Merge - by installing the original game, then the merge up to the compatible version mentioned (if the newest one by Rodril is newer, please send me an email to templayer@seznam.cz and I will update the community branch) and the newest GrayFace patch on top of that (link is in the first post in the thread)
1. Delete the script folder (we had problems with rogue lua files before...)
2. Copy paste the folders from GitLab into your installation directory, replacing existing (except for the script folder mentioned in the previous point... that one will be added instead of replaced). The download button can be hard to find because it is just an icon without words, so here you go with a link instead: https://gitlab.com/templayer/mmmerge/-/archive/master/mmmerge-master.zip
3. Use and configure any optional content in the "Changes from the Vanilla (Base / non-community branch) Merge" (i.e. following) section
4. Play.

### Changes from the Vanilla (Base / non-community branch) Merge -
1. Icon - there is an icon contained within the branch, made by Templayer out of the Dracolich assets, which were made by GDSpectra, Jamesx and Templayer. Should be compatible with Windows XP, but still includes a 256x256 image variant for systems that support such high-res icons. (also GitLab is telling me that it only contains the 256x256, which is false...)
2. Bolster Monster modifications - Fanasilver made a modification for a less aggressive HP scaling called "Use a non-aggresive formula for HP calculation". Currently non-toggleable. Results:

| Bolster | HP coefficient (× Base Health) |
|---------|--------------------------------|
| 0%      | 1                              |
| 100%    | 2                              |
| 150%    | 4                              |
| 200%    | 9                              |

3. Debug "tent" button at character creation to tell you the indexes of your choices (currently non-toggleable) by majaczek
4. Zombies will become Undead when made through Character Creation in order not to have the Zombie status (currently non-toggleable) by majaczek
5. Racial Skills are automatically learned if a character was created by using the Character Creation screen (currently non-toggleable) by majaczek
6. Better Autobiographies for Characters Created During Character Creation by adding Race (currently non-toggleable) by majaczek - currently disabled, causes a semi-crash when more than one character is created
7. Druid Summoning ALPHA - By holding CTRL while casting a targetting Earth spell by a Druid, summons creatures instead. I won't give you a list of creatures yet, since those are probably going change. By wuxiangjinxing
8. UI Crash Hotfix by CkNoSFeRaTU


### TODOs -
1. Make the custom optional modifications toggleable. Ideally some txt table with "settings". For example: BolsterMonster_LessAggressiveHitPointsFormula - if true, Fanasilver's formula gets used instead of vanilla Merge.
List of things to make toggleable - Merge Request 1 (in history), Merge Request 2 (four things to make toggleable there - debug for a debug button, Zombies transmuting to Undead at Character Creation, Learning Racial Skills at Character Creation, Better Autobiographies for Custom Characters), Merge Request 9 - Druid Summoning 
2. Better Autobiographies for Characters Created During Character Creation by adding Race (currently non-toggleable) by majaczek - currently disabled, causes a semi-crash when more than one character is created. Someone fix it! :D
3. Implement stuff on the Implementation Queue on the Merge Tracker - https://goo.gl/ui24Bz
4. Finish Up Druid Summoning - Summoner.lua
5. Replace UI Crash Hotfix with a proper solution (probably one made by Rodril or GrayFace) when it comes out

### Community Branch Credits -
Maintenance: Templayer<br>
Coding: majaczek, fanasilver, CkNoSFeRaTU, wuxiangjinxing, cthscr

